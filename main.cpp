#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <queue>
#include <cstdlib>
#include <vector>
#include <tuple>
#include <functional>
#include <climits>
using namespace std;

struct Dane
{
    Dane(int r, int p, int q): r(r), p(p), q(q){}

public:
	int r,p,q;
};

struct argmin
{
	bool operator()(Dane & pierwszy, Dane & drugi)
	{
	    if(pierwszy.r > drugi.r)
            return true;
        return false;
	}
};

struct argmax
{
	bool operator()(Dane & pierwszy, Dane & drugi)
	{
	    if(pierwszy.q < drugi.q)
            return true;
        return false;
	}
};

vector<Dane> permutacjaSchrageBezPodzialu(vector<Dane> data)
{
    vector<Dane> permutacja;

	priority_queue<Dane,vector<Dane>,argmin> N;
	priority_queue<Dane,vector<Dane>,argmax> G;

    int n = data.size();

    for(int i=0;i<n;i++)
    {
        Dane pom(data[i].r, data[i].p, data[i].q);
        N.push(pom);
    }

    int t = 0;
    Dane pi(0, 0, 0);
    while(!N.empty() || !G.empty())
    {
        while(!N.empty() && N.top().r <= t)
        {
            N.pop();
            G.push(N.top());
        }
        if(!G.size())
        {
            t = (N.top()).r;
        }
        else
        {
            Dane pom = G.top();
            permutacja.push_back(G.top());
            t = t + G.top().p;
            G.pop();
        }
    }

  return permutacja;
}

int CMaxBezPodzialu(vector<Dane> permutacja)
{
    int t = 0;
    int cMaxSBP = 0;
    Dane pi(0,0,0);

    int n = permutacja.size();

    for(int i = 0; i < n; i++)
    {
        t = max(t, permutacja[i].r);

        t = t + permutacja[i].p;
        cMaxSBP = max(cMaxSBP, t + permutacja[i].q);
    }

    return cMaxSBP;
}

int CMaxZPodzialem(vector<Dane> data)
{
	priority_queue<Dane,vector<Dane>,argmin> N;
	priority_queue<Dane,vector<Dane>,argmax> G;
    int n = data.size();
    while(n--)
    {
        Dane pom(data[n].r, data[n].p, data[n].q);
        N.push(pom);
    }
    int cMaxZP = -1;
    int t = 0;
    Dane pi(0, 0, 0);

    while(!N.empty() || !G.empty())
    {
        while(!N.empty() && (N.top()).r <= t)
        {
            G.push(N.top());
            N.pop();

            if(G.top().q > pi.q)
            {
                pi.p = t - G.top().r;
                t = G.top().r;
                if(pi.p > 0)
                    G.push(pi);
            }
        }

        if(G.empty())
        {
            t = N.top().r;
        }
        else
        {
            G.pop();
            t = t + G.top().p;
            cMaxZP = max(cMaxZP, t + G.top().q);
        }
    }

    return cMaxZP;
}

tuple<int, int> ABOblicz(vector<Dane> permutacja)
{
    int a = -1;
    int b = -1;

    int t = 0;
    int CMax = 0;
    int CMaxSBP = CMaxBezPodzialu(permutacja);
    Dane pi(0,0,0);

    int n = permutacja.size();

    for(int i = 0; i < n; i++)
    {
        t = max(t, permutacja[i].r);
        t = t + permutacja[i].p;

        if((t + permutacja[i].q) == CMaxSBP)
        {
            b = i;
        }

    }


    for(int i = 0; i < n; i++)
    {
        t = max(t, permutacja[i].r);

        t = t + permutacja[i].p;


        if(a == -1)
        {
            int suma = 0;
            for(int j = i; j <= b; j++)
            {
                suma += (permutacja[j].p);
            }
            suma += permutacja[b].q;
            if(CMaxSBP == permutacja[i].r + suma)
                a = i;
        }

    }

    return make_tuple(a,b);
}

int ObliczC(int a, int b, vector<Dane> permutacja)
{
    int c=-1;
    for(int i = a; i <= b; i++)
    {
        if(permutacja[i].q < permutacja[b].q)
        c = i;
    }
    return c;
}


void Carlier(vector<Dane> &dane, int &UB)
{
    vector<Dane> PI = permutacjaSchrageBezPodzialu(dane);

    int U = CMaxBezPodzialu(PI);//wynik schrage bez

    if(U < UB)
        UB = U;

    auto blokAB = ABOblicz(PI);
    int a = get<0>(blokAB);
    int b = get<1>(blokAB);
    int c = ObliczC(a,b,PI);


    if(c == -1)
        return;

    int rPRIM = 9999999;
    int qPRIM = 9999999;
    int pPRIM = 0;

    for(int i = c + 1; i <= b; i++)
    {
        if(PI[i].r < rPRIM)
            rPRIM = PI[i].r;

        if(PI[i].q < qPRIM)
            qPRIM = PI[i].q;

        pPRIM += PI[i].p;
    }

    int rPom = PI[c].r;
    PI[c].r = max(PI[c].r, rPRIM + pPRIM);

    int LB = CMaxZPodzialem(PI);

    if(LB < UB)
        Carlier(PI, UB);

    PI[c].r = rPom;

    int qPom = PI[c].q;
    PI[c].q = max(PI[c].q, qPRIM + pPRIM);
    LB = CMaxZPodzialem(PI);

    if(LB < UB)
        Carlier(PI, UB);

    PI[c].q = qPom;
}



int main()
{
    //char nazwaPliku[50] = "SCHRAGE1.DAT";
    //char nazwaPliku[50] = "SCHRAGE2.DAT";
    //char nazwaPliku[50] = "SCHRAGE3.DAT";
    //char nazwaPliku[50] = "SCHRAGE4.DAT";
    //char nazwaPliku[50] = "SCHRAGE5.DAT";
    char nazwaPliku[50] = "SCHRAGE6.DAT";
    //char nazwaPliku[50] = "SCHRAGE7.DAT";
    //char nazwaPliku[50] = "SCHRAGE8.DAT";
    //char nazwaPliku[50] = "SCHRAGE9.DAT";
    //char nazwaPliku[50] = "WT.DAT";

	ifstream plik( nazwaPliku, ios::in);

    int n;
    vector<Dane> input;
    Dane j(0,0,0);
	plik >> n;

	for (int i = 0; i < n; i++)
	{
        plik >> j.r >> j.p >> j.q;
        input.push_back(j);
    }

    int cMax = 1000000;
    Carlier(input, cMax);

    cout<< cMax << endl;

	system("PAUSE");
	return 0;
}
